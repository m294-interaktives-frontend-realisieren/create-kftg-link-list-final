const kftgLinks = [
  { text: 'Kanti Frauenfeld', href: 'https://www.kanti-frauenfeld.ch/' },
  {
    text: 'Stundenpläne',
    href: 'https://www.kanti-frauenfeld.ch/ueber-uns/stundenplaene.html/13550',
  },
  {
    text: 'Gymnasium',
    href: 'https://www.kanti-frauenfeld.ch/gymnasium.html/11476',
  },
  {
    text: 'Fachmittelschule',
    href: 'https://www.kanti-frauenfeld.ch/fachmittelschule.html/11477',
  },
  {
    text: 'Informatikmittelschule',
    href: 'https://www.kanti-frauenfeld.ch/informatikmittelschule.html/11479',
  },
  {
    text: 'Sonderaktivitäten IMS',
    href: 'https://www.kanti-frauenfeld.ch/informatikmittelschule/sonderaktivitaeten.html/11567',
  },
  {
    text: 'Fächer IMS',
    href: 'https://www.kanti-frauenfeld.ch/informatikmittelschule/faecher.html/11565',
  },
];

const elBody = document.querySelector('body');
const elScript = document.querySelector('body script');
// Listen Element für Links erstellen und
// vor script Element in Body hinzufügen
const elList = document.createElement('ul');
elBody.insertBefore(elList, elScript);

// Links mit Beschreibung erzeugen und in Listen Items (li) der Liste hinzufügen
for (let i = 0; i < kftgLinks.length; i++) {
  const kftgLink = kftgLinks[i];
  // Erzeugen von Listen Item Element und hinzufügen zu Listen Element
  const elListItem = document.createElement('li');
  elList.appendChild(elListItem);
  // Erzeugen von Link Element und hinzufügen zu Listen Item Element
  const elLink = document.createElement('a');
  elListItem.appendChild(elLink);
  // Attribute und Inhalt definieren
  elLink.setAttribute('href', kftgLink.href);
  elLink.setAttribute('target', '_blank');
  elLink.textContent = kftgLink.text;
}
